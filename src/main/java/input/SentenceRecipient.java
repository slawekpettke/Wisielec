package input;

import java.util.Scanner;

public class SentenceRecipient {
    Scanner scanner = new Scanner(System.in);
    public String obtainSentence() {
        return scanner.nextLine();
    }
}
