package input;

import wordsOperations.ArrayFromStringConverter;

import java.util.Scanner;

public class SignRecipient {
    Scanner scanner = new Scanner(System.in);
    public String obtainSign() {
        ArrayFromStringConverter convert = new ArrayFromStringConverter();
        return convert.stringToArray(scanner.next()).get(0);
    }
}
