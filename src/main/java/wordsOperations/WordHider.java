package wordsOperations;

import java.util.ArrayList;
import java.util.List;

public class WordHider {

    public ArrayList<String> doHidden(List<String> array){
        ArrayList<String> temp = new ArrayList<>();
        for (String e:array) {
            if(e.equals(" ")) {
                temp.add(" ");
            }else temp.add("_");
        }
        return temp;
    }
}
