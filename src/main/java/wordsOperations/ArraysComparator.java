package wordsOperations;

import java.util.List;

public class ArraysComparator {

    public boolean areSame(List<String> firstArray, List<String> secondArray) {
        boolean result = true;
        if (firstArray.size() != secondArray.size()) {
            result = false;
        }
        for (int i = 0; i < firstArray.size(); i++) {
            if (!firstArray.get(i).equals(secondArray.get(i))) {
                result = false;
            }
        }
        return result;
    }
}
