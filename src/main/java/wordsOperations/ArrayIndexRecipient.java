package wordsOperations;

import java.util.ArrayList;
import java.util.List;

public class ArrayIndexRecipient {
    public List<Integer> getLettersIndexes (List<String> array, String letter) {
        List<Integer> temp = new ArrayList<>();
        for (int i = 0; i < array.size(); i++) {
            if(array.get(i).equals(letter)){
                temp.add(i);
            }
        }
        return temp;
    }
}
