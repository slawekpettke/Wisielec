package wordsOperations;

import gallow.Gallow;

public class ArrayCleaner {

    private String emptyCell = "   ";
    public Gallow clear() {
        Gallow gallow = new Gallow();
        for (int i = 0; i < gallow.getSize(); i++) {
            for (int j = 0; j < gallow.getSize(); j++) {
                gallow.fillCell(i, j, emptyCell);
            }
        }
        return gallow;
    }
}
