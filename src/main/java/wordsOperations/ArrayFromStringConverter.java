package wordsOperations;

import java.util.ArrayList;

public class ArrayFromStringConverter {

    public ArrayList<String> stringToArray(String word) {
        ArrayList<String> temp = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            temp.add(String.valueOf(word.charAt(i)));
        }
        return temp;
    }
}
