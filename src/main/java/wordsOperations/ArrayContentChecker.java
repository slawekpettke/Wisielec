package wordsOperations;

import java.util.List;

public class ArrayContentChecker {

    public boolean isInArray (List<String> array, String letter){
        return array.contains(letter);
    }
}
