package messagesPrinter;

public class Messages {

    public static final String INTRODUCTION = "\n------Welcome in game \"Gallow\"------\n";
    public static final String GAME_RULES = "Guess your friend's password in the fewest attempts possible.\n\n" +
            "You should enter one letter or number at a time.\n" +
            "If you enter more than one letter or number, only first of them will be accepted.\n" +
            "You can make 6 mistakes. Each mistake adds a gallow element.\n" +
            "You win if you guess the password before building the gallow.\n";


    public static final String INFO_FOR_FIRST_PLAYER = "Player 1: Please input a password";
    public static final String INFO_FOR_SECOND_PLAYER = "Player 2: Please input a letter";
    public static final String HIDDEN_PASSWORD_COMMENT = "Password to guess: ";
    public static final String MISSED_LETTERS_COMMENTS = "Incorrectly guessed letters: ";
    public static final String LOST_DUE_TO_NO_ATTEMPTS = "YOU HAVE BEEN HANGED !!! \n\n" + "The password has not been guessed.\n" + "Password: ";
    public static final String CONGRATULATIONS = "CONGRATULATIONS !!!!\n";
    public static final String GUESSED_PASSWORD = "You have successfully guesssed the password.\n" + "PASSWORD: ";

    public static final String IF_PASSWORD_IS_EMPTY = "You DID NOT enter passwors. Please try again.\n";
}
