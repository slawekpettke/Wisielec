package GameFiles;

import wordsOperations.*;
import gallow.Gallow;
import gallow.GallowBuilder;
import input.SignRecipient;
import input.SentenceRecipient;
import messagesPrinter.Messages;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private static final int MISTAKES_LIMIT = 7;
    private ArrayCleaner cleanArray = new ArrayCleaner();
    private Gallow gallow = cleanArray.clear();
    private SentenceRecipient sentenceRecipient = new SentenceRecipient();
    private SignRecipient singRecipient = new SignRecipient();
    private ArrayFromStringConverter convertStringToArray = new ArrayFromStringConverter();
    private WordHider toHidden = new WordHider();
    private ArraysComparator arraysComparator = new ArraysComparator();
    private ArrayContentChecker arrayChecker = new ArrayContentChecker();
    private ArrayIndexRecipient indexesRecipient = new ArrayIndexRecipient();
    private SignTransferOpreator transferOperator = new SignTransferOpreator();
    private GallowBuilder gallowBuilder = new GallowBuilder();
    private List<String> password = new ArrayList<>();
    private List<String> hiddenPassword = new ArrayList<>();
    private List<Integer> indexesList = new ArrayList<>();
    private List<String> wrongSigns = new ArrayList<>();
    private boolean isFirstTime = true;
    private boolean isNull = true;
    private int failsCounter = 0;
    private String sign;


    public void start() {

        System.out.println(Messages.INTRODUCTION);
        System.out.println(Messages.GAME_RULES);

        getPassword();

        hiddenPassword = toHidden.doHidden(password);
        System.out.print(Messages.HIDDEN_PASSWORD_COMMENT);
        hiddenPassword.forEach(System.out::print);
        System.out.println();

        do {
            printGallowBottom();

            System.out.println(Messages.INFO_FOR_SECOND_PLAYER);
            sign = singRecipient.obtainSign();
            checkAndAssign();
            gallowBuilder.build(failsCounter, gallow);
            gallow.print();

            if (noAvailableAttempts()) break;

            System.out.println();
            System.out.print(Messages.HIDDEN_PASSWORD_COMMENT);
            hiddenPassword.forEach(System.out::print);
            System.out.println("\n");
            System.out.print(Messages.MISSED_LETTERS_COMMENTS);
            wrongSigns.forEach(System.out::print);
            System.out.println("\n");
        }
        while (failsCounter < MISTAKES_LIMIT && !arraysComparator.areSame(password, hiddenPassword));
        afterWinSummary();
    }


    private void getPassword() {
        while (isNull) {
            System.out.println(Messages.INFO_FOR_FIRST_PLAYER);
            password = convertStringToArray.stringToArray(sentenceRecipient.obtainSentence());
            if (!password.isEmpty()) {
                isNull = false;
            } else {
                System.err.println(Messages.IF_PASSWORD_IS_EMPTY);
            }
        }
    }

    private void afterWinSummary() {
        if (arraysComparator.areSame(password, hiddenPassword)) {
            System.out.println(Messages.CONGRATULATIONS);
            System.out.print(Messages.GUESSED_PASSWORD);
            password.forEach(System.out::print);
            System.out.println();
        }
    }

    private boolean noAvailableAttempts() {
        if (failsCounter == MISTAKES_LIMIT) {
            System.out.print(Messages.LOST_DUE_TO_NO_ATTEMPTS);
            password.forEach(System.out::print);
            System.out.println();
            return true;
        }
        return false;
    }

    private void printGallowBottom() {
        if (isFirstTime) {
            gallowBuilder.build(failsCounter, gallow);
            gallow.print();
            isFirstTime = false;
        }
    }

    private void checkAndAssign() {
        if (arrayChecker.isInArray(password, sign)) {
            indexesList = indexesRecipient.getLettersIndexes(password, sign);
            transferOperator.transferGuessLetters(indexesList, password, hiddenPassword);
        } else {
            wrongSigns.add(sign);
            failsCounter++;
        }
    }
}
