package gallow;

public class GallowParts {
    private String bottom = " _ ";
    private String emptyCell = "   ";
    private String verticalBeam = " | ";
    private String horizontalBeam = " - ";
    private String bodyShape = " | ";
    private String head = " O ";
    private String hands = "/|\\";
    private String legs = "/ \\";
    private int upperBeamLength = 6;
    private int ropeLength = 1;
    private int headLocation = 2;
    private int bodyStart = 3;
    private int bodyLength = 2;
    private int legsLocation = 5;



    public void addBase(Gallow gallow) {
//        clear(gallow);
        for (int i = gallow.getSize() - 1; i < gallow.getSize(); i++) {
            for (int j = 0; j < gallow.getSize(); j++) {
                gallow.fillCell(i, j, bottom);
            }
        }
    }

    public void addVerticalBeam(Gallow gallow) {
        for (int i = 0; i < gallow.getSize() - 1; i++) {
            for (int j = 0; j < 1; j++) {
                gallow.fillCell(i, j, verticalBeam);
            }
        }
    }

    public void addUpperBeam(Gallow gallow) {
        for (int i = 0; i < 1; i++) {
            for (int j = 1; j < upperBeamLength; j++) {
                gallow.fillCell(i, j, horizontalBeam);
            }
        }
    }

    public void addRope(Gallow gallow) {
        for (int i = 1; i <= ropeLength ; i++) {
            for (int j = upperBeamLength - 1; j <= upperBeamLength - 1; j++) {
                gallow.fillCell(i, j, verticalBeam);
            }
        }
    }

    public void addHead(Gallow gallow) {
        gallow.fillCell(headLocation, upperBeamLength - 1, head);
    }

    public void addBody(Gallow gallow) {
        for (int i = bodyStart; i < 6; i++) {
            for (int j = upperBeamLength - 1; j <= upperBeamLength - 1; j++) {
                gallow.fillCell(i, j, bodyShape);
            }
        }
    }

    public void addHands(Gallow gallow){
        gallow.fillCell(bodyStart, upperBeamLength -1, hands);
    }

    public void addLegs(Gallow gallow){
        gallow.fillCell(legsLocation, upperBeamLength-1, legs);
    }


}
