package gallow;

public class Gallow {

    private int arraySize = 10;

    private String [][] gallow = new String[arraySize][arraySize];

    public void fillCell (int row, int column, String value) {
        this.gallow [row][column] = value;
    }

    public void print () {
        for (int i = 0; i < this.gallow.length; i++) {
            for (int j = 0; j < this.gallow.length; j++) {
                System.out.print(this.gallow[i][j]);
            }
            System.out.println();
        }
    }

    public int getSize(){
        return this.gallow.length;
    }




}
