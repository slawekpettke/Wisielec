package gallow;

public class GallowBuilder {
    GallowParts printer = new GallowParts();
    public void build(int counter, Gallow gallow) {
        switch (counter) {
            case 0: {
                printer.addBase(gallow);
                break;
            }
            case 1: {
                printer.addVerticalBeam(gallow);
                break;
            }
            case 2: {
                printer.addUpperBeam(gallow);
                break;
            }
            case 3: {
                printer.addRope(gallow);
                break;
            }
            case 4: {
                printer.addHead(gallow);
                break;
            }
            case 5: {
                printer.addBody(gallow);
                break;
            }
            case 6: {
                printer.addHands(gallow);
                break;
            }
            case 7: {
                printer.addLegs(gallow);
                break;
            }

        }
    }

}

